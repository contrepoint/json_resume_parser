Rails.application.routes.draw do
	root 'resumes#new'
	resources :resumes, only: [:create, :index]
end
