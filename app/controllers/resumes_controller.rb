class ResumesController < ApplicationController

	def index
		if @resume == nil
			redirect_to root_path
		end
	end

	def new
		@resume = Resume.new
	end

	def create
		file = params[:resume][:file]
		@resume = JSON.parse(file.read).to_a
		@basics = @resume[0]
		education = @resume[1][1]
		@mhc = education[0]
			@mhc_awards = @mhc["Awards"].first
		@next_academy = education[1]
			@next_apps = @next_academy["Apps"].first
		@exp_corporate  = @resume[2]
		@exp_educator = @resume[3]
		@skills = @resume[4]
		render 'index'
	end

end
