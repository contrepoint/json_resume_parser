# JSON Resume Parser
* Upload a JSON file with my resume
* App will parse the JSON data and display it
* Reflections on the code [here](https://codingincounterpoint.wordpress.com/2016/04/15/json-resume-parser/)

# To Use
## On Heroku
* Go to the Heroku site [here](https://json-resume-parser.herokuapp.com/)
* Get the JSON resume [here](https://bitbucket.org/contrepoint/json_resume_parser/src/f4db9b7f554d5b4e2104810c6e9cddc6db10fe40/janice_resume.json?at=master&fileviewer=file-view-default)
* Upload the JSON resume to the site

## On your own computer
* `git clone https://bitbucket.org/contrepoint/json_resume_parser`
* `bundle install` (Install all gems)
* `bundle exec rake db:create` (Create the database)
* `bundle exec rake db:migrate` (Migrate the database)
* `bundle exec rails s` (Start the rails server)
* Go to localhost:3000
* Upload the JSON resume (janice_resume.json in the root folder)


## Resumes#new
`<%= f.file_field(:file, accept: 'application/json') %>` ensures that only json files can be selected to upload